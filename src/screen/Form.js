import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  TextInput,
  FlatList,
} from 'react-native';
import React, {useState} from 'react';
import CheckBox from '@react-native-community/checkbox';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

const Form = ({navigation, route}) => {
  const [merek, setMerek] = useState('');
  const [warna, setWarna] = useState('');
  const [ukuran, setUkuran] = useState('');
  const [catatan, setCatatan] = useState('');
  const [image, setImage] = useState('');
  const [store, setStore] = useState({});
  const [editId, setEditId] = useState('');
  const [editMode, setEditMode] = useState(false);
  const [options, setOptions] = useState([
    {
      name: 'Ganti Sol Sepatu',
      isChecked: false,
    },
    {
      name: 'Jahit Sepatu',
      isChecked: false,
    },
    {
      name: 'Repaint Sepatu',
      isChecked: false,
    },
    {
      name: 'Cuci Sepatu',
      isChecked: false,
    },
  ]);
  const {products} = useSelector(state => state.products);
  const dispatch = useDispatch();

  const handleCheckbox = item => {
    console.log('item  : ', item);
    const updateData = options.map(x =>
      x.name === item.name
        ? {...x, isChecked: !x.isChecked}
        : {...x, isChecked: false},
    );
    console.log('data : ', updateData);
    setOptions(updateData);
  };

  const getService = () => {
    const service = options.filter(item => item.isChecked);
    return service[0].name;
  };

  const storeData = () => {
    const dataProduct = [...products];
    const data = {
      idCart: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      merk: merek,
      color: warna,
      imageProduct: image,
      size: ukuran,
      service: getService(),
      note: catatan,
    };
    console.log(data);
    dataProduct.push(data);
    dispatch({type: 'ADD_PRODUCT', data: dataProduct});
    navigation.navigate('keranjang');
  };

  const editData = () => {
    const data = {
      idCart: editId,
      merk: merek,
      color: warna,
      imageProduct: image,
      size: ukuran,
      service: getService(),
      note: catatan,
    };
    dispatch({type: 'UPDATE_PRODUCT', data: data});
    navigation.navigate('keranjang');
  };

  React.useEffect(() => {
    if (route.params) {
      setEditMode(route.params.edit);
      const item = route.params.item;
      setEditId(item.idCart);
      setMerek(item.merk);
      setWarna(item.color);
      setUkuran(item.size);
      setCatatan(item.note);
      setImage(item.imageProduct);
      const updateData = options.map(x =>
        x.name === item.service
          ? {...x, isChecked: !x.isChecked}
          : {...x, isChecked: false},
      );
      setOptions(updateData);
    }
  }, []);

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 15,
            paddingVertical: 10,
            shadowOpacity: 1,
            shadowRadius: 4,
            shadowColor: 'black',
            elevation: 4,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              style={{marginRight: 10}}
              source={require('../assets/icon/black_back_arrow_ic.png')}
            />
          </TouchableOpacity>

          <Text
            style={{
              color: '#201F26',
              fontSize: 18,
              fontWeight: '700',
              tintColor: 'black',
            }}>
            Formulir Pemesanan
          </Text>
        </View>
        <View style={{marginTop: 30, marginHorizontal: 20}}>
          <Text style={{color: '#BB2427', fontSize: 12, fontWeight: '600'}}>
            Merek
          </Text>
          <TextInput
            placeholder="Masukkan Merk barang"
            value={merek}
            onChangeText={text => setMerek(text)}
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Warna
          </Text>
          <TextInput
            placeholder="Warna Barang, cth : Merah - Putih "
            value={warna}
            onChangeText={text => setWarna(text)}
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Ukuran
          </Text>
          <TextInput
            placeholder="Cth : S, M, L / 39,40"
            value={ukuran}
            onChangeText={text => setUkuran(text)}
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 15,
            }}>
            Photo
          </Text>
          <TextInput
            placeholder="Url Image"
            value={image}
            onChangeText={text => setImage(text)}
            style={{
              marginTop: 15,
              width: '100%',
              borderRadius: 8,
              backgroundColor: '#F6F8FF',
              paddingHorizontal: 10,
            }}
          />

          <FlatList
            data={options}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <TouchableOpacity
                  style={{
                    marginTop: 10,
                    borderColor: 'black',
                    borderWidth: 1,
                    height: 24,
                    width: 24,
                  }}
                  onPress={() => handleCheckbox(item)}>
                  {item.isChecked ? (
                    <Icon name="check" size={25} color="#000000" />
                  ) : null}
                </TouchableOpacity>
                <Text
                  style={{
                    color: '#201F26',
                    fontSize: 14,
                    fontWeight: '400',
                    marginTop: 10,
                    marginLeft: 15,
                  }}>
                  {item.name}
                </Text>
              </View>
            )}
          />

          <Text
            style={{
              color: '#BB2427',
              fontWeight: '600',
              fontSize: 12,
              marginTop: 20,
            }}>
            Catatan
          </Text>
          <View
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 10,
              marginVertical: 10,
              paddingHorizontal: 5,
            }}>
            <TextInput
              value={catatan}
              onChangeText={text => setCatatan(text)}
              style={{textAlignVertical: 'top', height: 93}}
              placeholder="Cth : ingin ganti sol baru"
              multiline={true}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            editMode ? editData() : storeData();
          }}
          style={{
            backgroundColor: '#BB2427',
            alignSelf: 'center',
            width: '90%',
            padding: 15,
            alignItems: 'center',
            marginHorizontal: 10,
            borderRadius: 10,
            marginBottom: 30,
          }}>
          <Text style={{fontSize: 16, fontWeight: '700', color: '#FFFFFF'}}>
            {editMode ? 'Edit Data' : 'Masukkan Keranjang'}
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Form;
