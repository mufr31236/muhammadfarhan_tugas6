import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';

const TransaksiDetail = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <View
        style={{
          paddingHorizontal: 15,
          paddingVertical: 15,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: '#DCDCDC40',
          elevation: 4,
          marginBottom: 5,
          backgroundColor: '#FFFFFF',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{marginRight: 10}}
            source={require('../assets/icon/black_back_arrow_ic.png')}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          marginTop: 5,
          alignItems: 'center',
          backgroundColor: 'white',
          paddingVertical: 10,
        }}>
        <Text style={{fontSize: 12, fontWeight: '500', color: '#BDBDBD'}}>
          20 Desember 2020 09:00
        </Text>
        <Text
          style={{
            color: '#201F26',
            fontSize: 36,
            fontWeight: '700',
            marginTop: 50,
          }}>
          CS122001
        </Text>
        <Text style={{color: '#000000', fontSize: 14, fontWeight: '400'}}>
          Kode Reservasi
        </Text>
        <Text style={{color: '#6F6F6F', marginTop: 20, textAlign: 'center'}}>
          Sebutkan Kode Reservasi saat tiba di outlet
        </Text>
      </View>
      <Text
        style={{
          color: '#201F26',
          fontSize: 14,
          fontWeight: '400',
          marginLeft: 20,
          marginVertical: 10,
        }}>
        Barang
      </Text>
      <View
        style={{
          backgroundColor: 'white',
          marginVertical: 5,
          marginHorizontal: 10,
          flexDirection: 'row',
          borderRadius: 10,
          paddingVertical: 20,
          paddingHorizontal: 15,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: 'black',
          elevation: 4,
        }}>
        <Image
          style={{height: 84, width: 84, marginRight: 10}}
          source={require('../assets/image/product_img.png')}
        />
        <View>
          <Text
            style={{
              color: '#000000',
              fontSize: 12,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            New Balance - Pink Abu - 40
          </Text>
          <Text
            style={{
              color: '#737373',
              fontSize: 12,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            Cuci Sepatu
          </Text>
          <Text
            style={{
              color: '#737373',
              fontSize: 12,
              fontWeight: '500',
              marginVertical: 5,
            }}>
            Note : -
          </Text>
        </View>
      </View>
      <Text
        style={{
          color: '#201F26',
          fontSize: 14,
          fontWeight: '400',
          marginLeft: 20,
          marginTop: 20,
          marginBottom: 10,
        }}>
        Barang
      </Text>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('checkout');
        }}>
        <View
          style={{
            backgroundColor: 'white',
            marginVertical: 5,
            marginHorizontal: 10,
            flexDirection: 'row',
            borderRadius: 10,
            paddingVertical: 20,
            paddingHorizontal: 15,
            shadowOpacity: 1,
            shadowRadius: 4,
            shadowColor: 'black',
            elevation: 4,
          }}>
          <Image
            source={require('../assets/icon/status_ic.png')}
            style={{height: 14, width: 14, marginTop: 5}}
          />
          <View
            style={{
              flexDirection: 'column',
              width: '80%',
              paddingHorizontal: 10,
            }}>
            <Text style={{color: '#201F26', fontSize: 14, fontWeight: '400'}}>
              Telah Reservasi
            </Text>
            <Text style={{color: '#A5A5A5', fontSize: 10, fontWeight: '400'}}>
              20 Desember 2020
            </Text>
          </View>
          <Text
            style={{
              color: '#A5A5A5',
              fontSize: 10,
              fontWeight: '400',
              marginTop: 5,
            }}>
            09:00
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default TransaksiDetail;
