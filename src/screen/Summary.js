import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useState} from 'react';
import moment from 'moment/moment';

const Summary = ({navigation, route}) => {
  const dispatch = useDispatch();

  const {reservations} = useSelector(state => state.reservation);
  const {products} = useSelector(state => state.products);
  const {store} = useSelector(state => state.store);

  React.useEffect(() => {
    console.log('Check options : ', reservations);
    console.log('check product: ', products);
  }, []);

  const reservasi = () => {
    const data = {
      idTransaction: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      reservationCode: moment(new Date()).format('DDHHmmss'),
      customerData: {
        nama: 'Agil Bani',
        email: 'gantengdoang@dipanggang.com',
        phoneNumber: '0813763476',
        address: 'Jl. Perumnas, Condong catur, Sleman, Yogyakarta',
      },
      storeData: store[0],
      cartData: products,
    };
    console.log('Check data : ', data);
    console.log('reservation: ', reservations);
    reservations.push(data);

    dispatch({type: 'DELETE_PRODUCT'});
    dispatch({type: 'ADD_RESERVATION', data: reservations});
  };

  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 15,
          paddingVertical: 15,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: '#DCDCDC40',
          elevation: 4,
          marginBottom: 5,
          backgroundColor: '#FFFFFF',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{marginRight: 10}}
            source={require('../assets/icon/black_back_arrow_ic.png')}
          />
        </TouchableOpacity>

        <Text
          style={{
            color: '#201F26',
            fontSize: 18,
            fontWeight: '700',
            tintColor: 'black',
          }}>
          Summary
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          paddingVertical: 10,
          paddingHorizontal: 20,
        }}>
        <Text
          style={{
            color: '#979797',
            fontSize: 14,
            fontWeight: '500',
            marginVertical: 5,
          }}>
          Data Customer
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          Agil Bani (0813763476)
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          Jl. Perumnas, Condong catur, Sleman, Yogyakarta
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          gantengdoang@dipanggang.com
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          paddingVertical: 10,
          paddingHorizontal: 20,
        }}>
        <Text
          style={{
            color: '#979797',
            fontSize: 14,
            fontWeight: '500',
            marginVertical: 5,
          }}>
          Alamat Outlet Tujuan
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          {`${store[0].storeName}`}
        </Text>
        <Text style={{color: '#201F26', marginVertical: 5}}>
          {store[0].address}
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          paddingVertical: 10,
          paddingHorizontal: 20,
        }}>
        <Text
          style={{
            color: '#979797',
            fontSize: 14,
            fontWeight: '500',
            marginVertical: 5,
          }}>
          Barang
        </Text>
        {products.map((item, index) => (
          <View
            style={{
              backgroundColor: 'white',
              flexDirection: 'row',
              borderRadius: 10,
            }}>
            <Image
              style={{height: 84, width: 84, marginRight: 10}}
              source={{uri: item.imageProduct}}
            />
            <View style={{marginLeft: 10}}>
              <Text
                style={{
                  color: '#000000',
                  fontSize: 12,
                  fontWeight: '500',
                  marginVertical: 5,
                }}>
                {item.merk}
              </Text>
              <Text
                style={{
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '500',
                  marginVertical: 5,
                }}>
                {item.service}
              </Text>
              <Text
                style={{
                  color: '#737373',
                  fontSize: 12,
                  fontWeight: '500',
                  marginVertical: 5,
                }}>
                {'Note : ' + item.note}
              </Text>
            </View>
          </View>
        ))}
      </View>

      <TouchableOpacity
        onPress={() => {
          reservasi();
          navigation.navigate('sukses');
        }}
        style={{
          position: 'absolute',
          bottom: 45,
          backgroundColor: '#BB2427',
          alignSelf: 'center',
          width: '90%',
          padding: 15,
          alignItems: 'center',
          marginHorizontal: 10,
          borderRadius: 10,
        }}>
        <Text style={{fontSize: 16, fontWeight: '700', color: '#FFFFFF'}}>
          Reservasi Sekarang
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Summary;
