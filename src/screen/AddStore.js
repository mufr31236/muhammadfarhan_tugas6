import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  FlatList,
  Button,
} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import {useDispatch, useSelector} from 'react-redux';

const AddStore = ({navigation, route}) => {
  const [namaToko, setNamaToko] = useState('');
  const [alamat, setAlamat] = useState('');
  const [rating, setRating] = useState(0.0);
  const [hargaMaks, setHargaMaks] = useState('');
  const [hargaMin, setHargaMin] = useState('');
  const [deskripsi, setDeskripsi] = useState('');
  const [imgToko, setImgToko] = useState('');
  const [status, setStatus] = useState(null);
  const [openTime, setOpenTime] = useState('');
  const [openTimePickerVisible, setOpenTimePickerVisible] = useState(false);
  const [closeTimePickerVisible, setCloseTimePickerVisible] = useState(false);
  const [closeTime, setCloseTime] = useState('');

  const {store} = useSelector(state => state.store);
  const dispatch = useDispatch();

  const showDatePicker = type => {
    console.log('trace error : ', type);
    if (type === 'OPEN') {
      setOpenTimePickerVisible(true);
    } else if (type === 'CLOSE') {
      setCloseTimePickerVisible(true);
    }
  };

  const hideDatePicker = type => {
    if (type === 'OPEN') {
      setOpenTimePickerVisible(false);
    } else if (type === 'CLOSE') {
      setCloseTimePickerVisible(false);
    }
  };

  const handleConfirm = (time, type) => {
    if (type === 'OPEN') {
      console.log('OPEN : ', time);
      setOpenTime(time);
    } else if (type === 'CLOSE') {
      setCloseTime(time);
    }
    hideDatePicker(type);
  };

  const getTime = type => {
    if (type === 'OPEN') {
      var tempTime = openTime.toString().split(' ');
      var time = tempTime[4].toString().split(':');
      return `${time[0]}:${time[1]}`;
    } else if (type === 'CLOSE') {
      var tempTime = closeTime.toString().split(' ');
      var time = tempTime[4].toString().split(':');
      return `${time[0]}:${time[1]}`;
    }
  };

  const addStore = () => {
    const newData = {
      idStore: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      address: alamat,
      availableTime: `${getTime('OPEN')} : ${getTime('CLOSE')}`,
      description: deskripsi,
      favourite: false,
      isOpen: status,
      minimumPrice: hargaMin,
      maximumPrice: hargaMaks,
      ratings: rating,
      storeImage: imgToko,
      storeName: namaToko,
    };
    store.push(newData);
    dispatch({type: 'ADD_STORE', data: store});
    navigation.goBack();
  };

  const checkBtn = () => {
    return (
      namaToko === '' ||
      alamat === '' ||
      rating <= 0 ||
      closeTime === '' ||
      openTime === '' ||
      hargaMin === '' ||
      hargaMaks === '' ||
      deskripsi === '' ||
      imgToko === ''
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white', margin: 10}}>
      <Text
        style={{margin: 5, fontSize: 16, color: 'black', fontWeight: 'bold'}}>
        Add Store Data
      </Text>

      <ScrollView>
        <View style={{paddingHorizontal: 15}}>
          <Text style={{color: 'black', marginVertical: 5}}>Nama Toko</Text>
          <TextInput
            value={namaToko}
            onChangeText={it => setNamaToko(it)}
            style={{
              height: 40,
              width: '95%',
              borderColor: 'gray',
              borderWidth: 1,
              marginBottom: 20,
              borderRadius: 5,
              paddingHorizontal: 10,
            }}
            placeholder="Masukkan Nama Toko"
          />

          <Text style={{color: 'black', marginVertical: 5}}>Alamat</Text>
          <TextInput
            value={alamat}
            onChangeText={it => setAlamat(it)}
            style={{
              height: 40,
              width: '95%',
              borderColor: 'gray',
              borderWidth: 1,
              marginBottom: 15,
              borderRadius: 5,
              paddingHorizontal: 10,
            }}
            placeholder="Masukkan Alamat Toko"
          />

          <Text style={{color: 'black', marginVertical: 5}}>
            Status Buka Tutup
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 10,
              paddingTop: 15,
            }}>
            <View style={{flexDirection: 'row', width: '50%'}}>
              <TouchableOpacity
                style={{
                  height: 24,
                  width: 24,
                  borderColor: 'black',
                  borderWidth: 1,
                  borderRadius: 25,
                  marginRight: 10,
                  backgroundColor: status ? 'grey' : 'white',
                }}
                onPress={() => {
                  setStatus(true);
                }}>
                {status ? <Icon name="check" size={20} color="white" /> : null}
              </TouchableOpacity>
              <Text style={{color: 'black'}}>Buka</Text>
            </View>
            <View style={{flexDirection: 'row', width: '50%'}}>
              <TouchableOpacity
                style={{
                  height: 24,
                  width: 24,
                  borderColor: 'black',
                  borderWidth: 1,
                  borderRadius: 25,
                  marginRight: 10,
                  backgroundColor:
                    status === false && status !== null ? 'grey' : 'white',
                }}
                onPress={() => {
                  setStatus(false);
                }}>
                {!status ? <Icon name="check" size={20} color="white" /> : null}
              </TouchableOpacity>
              <Text style={{color: 'black'}}>Tutup</Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: 10,
              paddingTop: 15,
              marginBottom: 10,
            }}>
            <View style={{width: '50%'}}>
              <Text
                style={{
                  color: 'black',
                  fontWeight: 'bold',
                  marginBottom: 10,
                }}>
                Jam Buka
              </Text>
              <TouchableOpacity
                onPress={() => showDatePicker('OPEN')}
                style={{
                  height: 30,
                  width: '80%',
                  borderColor: 'black',
                  borderWidth: 1,
                  alignItems: 'center',
                  paddingLeft: 10,
                  paddingRight: 5,
                  paddingTop: 2,
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      width: '80%',
                      fontSize: 16,
                      color: 'black',
                    }}>
                    {openTime ? getTime('OPEN') : 'Pilih Jam'}
                  </Text>
                  <Icon name="clock" solid style={{width: '20%'}} size={22} />
                </View>
              </TouchableOpacity>

              <DateTimePickerModal
                isVisible={openTimePickerVisible}
                mode="time"
                display="spinner"
                is24Hour
                locale="en_GB"
                onConfirm={time => handleConfirm(time, 'OPEN')}
                onCancel={() => hideDatePicker('OPEN')}
              />
            </View>
            <View style={{width: '50%'}}>
              <Text
                style={{
                  color: 'black',
                  fontWeight: 'bold',
                  marginBottom: 10,
                }}>
                Jam Tutup
              </Text>
              <TouchableOpacity
                onPress={() => showDatePicker('CLOSE')}
                style={{
                  height: 30,
                  width: '80%',
                  borderColor: 'black',
                  borderWidth: 1,
                  alignItems: 'center',
                  paddingLeft: 10,
                  paddingRight: 5,
                  paddingTop: 2,
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={{
                      width: '80%',
                      fontSize: 16,
                      color: 'black',
                    }}>
                    {closeTime ? getTime('CLOSE') : 'Pilih Jam'}
                  </Text>
                  <Icon name="clock" solid style={{width: '20%'}} size={22} />
                </View>
              </TouchableOpacity>

              <DateTimePickerModal
                isVisible={closeTimePickerVisible}
                mode="time"
                display="spinner"
                is24Hour
                locale="en_GB"
                onConfirm={time => handleConfirm(time, 'CLOSE')}
                onCancel={() => hideDatePicker('CLOSE')}
              />
            </View>
          </View>
          <Text style={{color: 'black', marginVertical: 5}}>Rating</Text>
          <TextInput
            value={rating}
            onChangeText={it => setRating(it)}
            style={{
              height: 40,
              width: '95%',
              borderColor: 'gray',
              borderWidth: 1,
              marginBottom: 10,
              borderRadius: 5,
              paddingHorizontal: 10,
            }}
            placeholder="Masukkan Rating Toko"
          />

          <View
            style={{
              flexDirection: 'row',
              marginVertical: 5,
              width: '95%',
            }}>
            <View style={{width: '50%'}}>
              <Text style={{color: 'black', marginBottom: 5}}>
                Harga Minimal
              </Text>
              <TextInput
                value={hargaMin}
                onChangeText={it => setHargaMin(it)}
                style={{
                  height: 40,
                  width: '95%',
                  borderColor: 'gray',
                  borderWidth: 1,
                  marginBottom: 10,
                  borderRadius: 5,
                  paddingHorizontal: 10,
                }}
              />
            </View>
            <View style={{width: '50%'}}>
              <Text style={{color: 'black', marginBottom: 5}}>
                Harga Maksimal
              </Text>
              <TextInput
                value={hargaMaks}
                onChangeText={it => setHargaMaks(it)}
                style={{
                  height: 40,
                  width: '95%',
                  borderColor: 'gray',
                  borderWidth: 1,
                  marginBottom: 10,
                  borderRadius: 5,
                  paddingHorizontal: 10,
                }}
              />
            </View>
          </View>

          <Text style={{color: 'black', marginVertical: 5}}>Deskripsi</Text>
          <TextInput
            value={deskripsi}
            onChangeText={it => setDeskripsi(it)}
            style={{
              height: 40,
              width: '95%',
              borderColor: 'gray',
              borderWidth: 1,
              marginBottom: 10,
              borderRadius: 5,
              paddingHorizontal: 10,
            }}
            placeholder="Masukkan Rating Toko"
          />

          <Text style={{color: 'black', marginVertical: 5}}>Gambar Toko</Text>
          <View
            style={{
              flexDirection: 'row',
              height: 40,
              width: '95%',
              borderColor: 'gray',
              borderWidth: 1,
              marginBottom: 10,
              borderRadius: 5,
              paddingHorizontal: 10,
            }}>
            {imgToko === '' ? (
              <Icon name="image" size={24} style={{marginTop: 5}} />
            ) : null}
            <TextInput
              value={imgToko}
              onChangeText={it => setImgToko(it)}
              placeholder="Masukkan Gambar"
            />
          </View>
          <TouchableOpacity
            disabled={checkBtn()}
            onPress={addStore}
            style={{
              width: '100%',
              backgroundColor: checkBtn() ? 'grey' : 'blue',
              padding: 10,
              alignItems: 'center',
              borderRadius: 10,
            }}>
            <Text style={{color: 'white'}}>SIMPAN</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default AddStore;
