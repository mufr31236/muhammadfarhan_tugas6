import {View, Text, Image} from 'react-native';
import React, {useEffect} from 'react';

const SplashScreen = ({navigation, route}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('authnavigation');
    }, 1000);
  });
  return (
    <View
      style={{
        backgroundColor: '#fff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={require('../assets/image/splash.png')}
        style={{width: 150, height: 150, resizeMode: 'contain'}}
      />
    </View>
  );
};

export default SplashScreen;
