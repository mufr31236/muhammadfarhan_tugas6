import {
  View,
  Text,
  Image,
  Dimensions,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {useState} from 'react';

const Detail = ({navigation, route}) => {
  const [item, setItem] = useState({});

  React.useEffect(() => {
    if (route.params) {
      setItem(route.params.item);
    }
  }, []);
  return (
    <View style={{flex: 1}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground
          source={require('../assets/image/detail_img.png')}
          style={{
            width: Dimensions.get('window').width,
            height: 316,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
              marginTop: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <Image
                style={{width: 24, height: 24}}
                source={require('../assets/icon/back_arrow_ic.png')}
              />
            </TouchableOpacity>

            <Image
              style={{width: 24, height: 24}}
              source={require('../assets/icon/white_bag_ic.png')}
            />
          </View>
        </ImageBackground>
        <View
          style={{
            width: '100%',
            backgroundColor: '#fff',
            borderTopLeftRadius: 19,
            borderTopRightRadius: 19,
            paddingHorizontal: 20,
            paddingTop: 20,
            marginTop: -10,
          }}>
          <Text
            style={{
              color: '#201F26',
              fontSize: 18,
              fontWeight: '700',
            }}>
            {item.storeName}
          </Text>
          <View style={{flexDirection: 'row', marginVertical: 5}}>
            <Image
              source={require('../assets/icon/star_ic.png')}
              style={{height: 10, width: 10}}
            />
            <Image
              source={require('../assets/icon/star_ic.png')}
              style={{height: 10, width: 10}}
            />
            <Image
              source={require('../assets/icon/star_ic.png')}
              style={{height: 10, width: 10}}
            />
            <Image
              source={require('../assets/icon/star_ic.png')}
              style={{height: 10, width: 10}}
            />
            <Image
              source={require('../assets/icon/empty_star_ic.png')}
              style={{height: 10, width: 10}}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 5,
              justifyContent: 'space-evenly',
            }}>
            <Image source={require('../assets/icon/location_ic.png')} />
            <Text>{item.address}</Text>
            <Text style={{color: '#3471CD'}}>Lihat Maps</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                borderRadius: 20,
                width: 60,
                backgroundColor: item.isOpen ? '#11A84E1F' : '#E64C3C33',
                alignItems: 'center',
                marginVertical: 5,
              }}>
              <Text
                style={{
                  color: item.isOpen ? '#11A84E' : '#EA3D3D',
                  paddingHorizontal: 5,
                  paddingVertical: 5,
                  fontSize: 12,
                }}>
                {item.isOpen ? `Buka` : `Tutup`}
              </Text>
            </View>
            <Text
              style={{
                color: '#343434',
                fontWeight: '700',
                fontSize: 12,
                marginVertical: 10,
                marginHorizontal: 10,
              }}>
              {item.availableTime}
            </Text>
          </View>
          <View style={{paddingVertical: 20}}>
            <Text style={{color: '#201F26', fontSize: 16, marginBottom: 5}}>
              Deskripsi
            </Text>
            <Text
              style={{
                fontWeight: '400',
                fontSize: 14,
                textAlign: 'justify',
                marginBottom: 10,
              }}>
              {item.description}
            </Text>
            <Text style={{color: '#201F26', fontSize: 16, marginBottom: 10}}>
              Range Biaya
            </Text>
            <Text style={{color: '#8D8D8D', fontSize: 16, marginBottom: 10}}>
              {`Rp. ${item.minimumPrice} - ${item.maximumPrice}`}
            </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('form', {item});
              }}
              style={{
                backgroundColor: '#BB2427',
                paddingVertical: 10,
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{color: '#FFFFFF', fontSize: 16}}>
                Repair Disini
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Detail;
