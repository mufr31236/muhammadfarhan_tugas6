import {View, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';

const Transaksi = ({navigation, route}) => {
  const {reservations} = useSelector(state => state.reservation);
  const dispatch = useDispatch();
  const deleteReservation = item => {
    dispatch({type: 'DELETE_RESERVATION', data: item});
  };

  const getMonthName = month => {
    if (month === '01') {
      return 'Januari';
    } else if (month === '02') {
      return 'Februari';
    } else if (month === '03') {
      return 'Maret';
    } else if (month === '04') {
      return 'April';
    } else if (month === '05') {
      return 'May';
    } else if (month === '06') {
      return 'Juni';
    } else if (month === '07') {
      return 'Juli';
    } else if (month === '08') {
      return 'Agustus';
    } else if (month === '09') {
      return 'September';
    } else if (month === '10') {
      return 'Oktober';
    } else if (month === '11') {
      return 'November';
    } else if (month === '12') {
      return 'Desember';
    }
  };

  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 15,
          paddingVertical: 15,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: '#DCDCDC40',
          elevation: 4,
          marginBottom: 5,
          backgroundColor: '#FFFFFF',
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{marginRight: 10}}
            source={require('../assets/icon/black_back_arrow_ic.png')}
          />
        </TouchableOpacity>

        <Text
          style={{
            color: '#201F26',
            fontSize: 18,
            fontWeight: '700',
            tintColor: 'black',
          }}>
          Transaksi
        </Text>
      </View>
      {reservations.map((transaction, index) => (
        <FlatList
          data={transaction.cartData}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('transactionnavigation', {
                  screen: 'detail',
                });
              }}>
              <View
                style={{
                  backgroundColor: 'white',
                  marginVertical: 5,
                  marginHorizontal: 10,
                  flexDirection: 'column',
                  borderRadius: 10,
                  paddingVertical: 20,
                  paddingHorizontal: 15,
                  shadowOpacity: 1,
                  shadowRadius: 4,
                  shadowColor: 'black',
                  elevation: 4,
                }}>
                <Text
                  style={{
                    color: '#BDBDBD',
                    fontSize: 12,
                    fontWeight: '500',
                    marginBottom: 10,
                  }}>
                  {`${transaction.idTransaction.substring(
                    6,
                    8,
                  )}  ${getMonthName(
                    transaction.idTransaction.substring(4, 6),
                  )}  ${transaction.idTransaction.substring(0, 4)}`}
                </Text>
                <Text
                  style={{
                    color: '#201F26',
                    fontSize: 12,
                    fontWeight: '500',
                    marginVertical: 5,
                  }}>
                  {item.merk}
                </Text>
                <Text
                  style={{
                    color: '#201F26',
                    fontWeight: '400',
                    fontSize: 12,
                  }}>
                  {`${item.service}`}
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    marginVertical: 10,
                  }}>
                  <Text
                    style={{color: '#201F26', fontSize: 12, fontWeight: '400'}}>
                    Kode Reservasi :
                    <Text
                      style={{
                        color: '#201F26',
                        fontSize: 12,
                        fontWeight: '700',
                      }}>
                      {`   ${transaction.reservationCode}`}
                    </Text>
                  </Text>
                  <View
                    style={{
                      flex: 1,
                    }}>
                    <View
                      style={{
                        backgroundColor: '#F29C1F29',
                        borderRadius: 15,
                        paddingHorizontal: 10,
                        alignSelf: 'flex-end',
                      }}>
                      <Text
                        style={{
                          color: '#FFC107',
                          fontWeight: '400',
                          fontSize: 12,
                        }}>
                        Reserved
                      </Text>
                    </View>
                  </View>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    deleteReservation(item);
                  }}
                  style={{
                    alignSelf: 'flex-end',
                    backgroundColor: 'red',
                    padding: 5,
                    borderRadius: 10,
                  }}>
                  <Text
                    style={{color: 'white', fontWeight: 'bold', fontSize: 12}}>
                    Delete
                  </Text>
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          )}
        />
      ))}
    </View>
  );
};

export default Transaksi;
