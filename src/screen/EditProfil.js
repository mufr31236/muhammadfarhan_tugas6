import {View, Text, Image, TouchableOpacity, TextInput} from 'react-native';
import React from 'react';

const EditProfil = ({navigation, route}) => {
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 15,
          paddingVertical: 10,
          shadowOpacity: 1,
          shadowRadius: 4,
          shadowColor: 'black',
          elevation: 4,
        }}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <Image
            style={{marginRight: 10}}
            source={require('../assets/icon/black_back_arrow_ic.png')}
          />
        </TouchableOpacity>

        <Text
          style={{
            color: '#201F26',
            fontSize: 18,
            fontWeight: '700',
            tintColor: 'black',
          }}>
          Edit Profile
        </Text>
      </View>
      <Image
        style={{
          height: 95,
          width: 95,
          alignSelf: 'center',
          marginTop: 30,
          marginBottom: 15,
        }}
        source={require('../assets/image/profile_img.png')}
      />
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 15,
          paddingVertical: 10,
          alignSelf: 'center',
        }}>
        <Image
          style={{height: 24, width: 24, marginRight: 10}}
          source={require('../assets/icon/edit_ic.png')}
        />
        <Text style={{color: '#3A4BE0', fontSize: 18, fontWeight: '500'}}>
          Edit Foto
        </Text>
      </View>
      <View style={{marginTop: 30, marginHorizontal: 20}}>
        <Text style={{color: '#BB2427', fontSize: 12, fontWeight: '600'}}>
          Nama
        </Text>
        <TextInput
          placeholder="Agil Bani"
          style={{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10,
          }}
        />
        <Text
          style={{
            color: '#BB2427',
            fontSize: 12,
            fontWeight: '600',
            marginTop: 15,
          }}>
          Email
        </Text>
        <TextInput
          placeholder="gilagil@gmail.com"
          style={{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10,
          }}
          keyboardType="email-address"
        />
        <Text
          style={{
            color: '#BB2427',
            fontSize: 12,
            fontWeight: '600',
            marginTop: 15,
          }}>
          No hp
        </Text>
        <TextInput
          placeholder="08124564879"
          style={{
            marginTop: 15,
            width: '100%',
            borderRadius: 8,
            backgroundColor: '#F6F8FF',
            paddingHorizontal: 10,
          }}
          keyboardType="email-address"
        />
      </View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('profile');
        }}
        style={{
          position: 'absolute',
          bottom: 45,
          backgroundColor: '#BB2427',
          alignSelf: 'center',
          width: '90%',
          padding: 15,
          alignItems: 'center',
          marginHorizontal: 10,
          borderRadius: 10,
        }}>
        <Text style={{fontSize: 16, fontWeight: '700', color: '#FFFFFF'}}>
          Simpan
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default EditProfil;
