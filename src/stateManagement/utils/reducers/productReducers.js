const initialState = {
  products: [],
};

const productsReducer = (state = initialState, action) => {
  let newProducts = [...state.products];
  switch (action.type) {
    case 'ADD_PRODUCT':
      return {
        ...state,
        products: action.data,
      };

    case 'UPDATE_PRODUCT':
      var newData = [...state.products];
      console.log('Check old data : ', newData);
      console.log('Check new input data : ', action.data);
      var findIndex = state.products.findIndex(it => {
        return it.idCart == action.data.idCart;
      });
      newData[findIndex] = action.data;
      return {
        ...state,
        products: newData,
      };

    case 'DELETE_PRODUCT':
      return {
        ...state,
        products: [],
      };
    default:
      return state;
  }
};

export default productsReducer;
