const initialState = {
  store: [],
};

const storeReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_STORE':
      return {
        ...state,
        store: action.data,
      };

    case 'DELETE_STORE':
      var newData = [...state.store];
      var findIndex = state.reservations.findIndex(it => {
        return it.id == action.data.id;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        store: newData,
      };

    case 'FAVORITE':
      var newData = [...state.store];
      var findIndex = state.store.findIndex(it => {
        return it.id == action.data.id;
      });
      var isFav = newData[findIndex].favorite;
      newData[findIndex].favorite = !isFav;
      return {
        ...state,
        store: newData,
      };

    default:
      return state;
  }
};

export default storeReducers;
