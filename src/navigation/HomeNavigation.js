import {View, Text} from 'react-native';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screen/Home';
import Detail from '../screen/Detail';
import Form from '../screen/Form';
import Keranjang from '../screen/Keranjang';
import Summary from '../screen/Summary';
import Succes from '../screen/Succes';
import AddStore from '../screen/AddStore';

const Stack = createStackNavigator();

const HomeNavigation = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="home" component={Home} />
      <Stack.Screen name="detail" component={Detail} />
      <Stack.Screen name="form" component={Form} />
      <Stack.Screen name="keranjang" component={Keranjang} />
      <Stack.Screen name="summary" component={Summary} />
      <Stack.Screen name="sukses" component={Succes} />
      <Stack.Screen name="addStore" component={AddStore} />
    </Stack.Navigator>
  );
};

export default HomeNavigation;
